package org.chinaunicom.permission.dao;

import org.chinaunicom.permission.entity.User;

public interface UserDao extends BaseDao<User>{

}
