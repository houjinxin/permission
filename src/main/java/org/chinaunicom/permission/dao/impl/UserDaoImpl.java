package org.chinaunicom.permission.dao.impl;

import org.chinaunicom.permission.dao.UserDao;
import org.chinaunicom.permission.entity.User;
import org.springframework.stereotype.Repository;

@Repository("userDao")
public class UserDaoImpl extends BaseDaoImpl<User> implements UserDao {


}
