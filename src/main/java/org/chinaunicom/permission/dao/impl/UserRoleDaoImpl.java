package org.chinaunicom.permission.dao.impl;

import java.util.List;
import java.util.Map;

import org.chinaunicom.permission.dao.UserRoleDao;
import org.chinaunicom.permission.entity.UserRole;
import org.springframework.stereotype.Repository;

@Repository("userRoleDao")
public class UserRoleDaoImpl extends BaseDaoImpl<UserRole> implements UserRoleDao{

	@Override
	public List<Map<String, Object>> queryRoleUser(int rid) {
		String sql = "select DISTINCT ur.uid,u.username from user_role ur, user u where ur.uid=u.uid and ur.rid=?";
		List<Map<String, Object>> roleUsers =  jdbcTemplate.queryForList(sql, rid);
		return roleUsers;
	}

	
}
