package org.chinaunicom.permission.dao.impl;

import java.util.List;
import java.util.Map;

import org.chinaunicom.permission.dao.RoleMenuDao;
import org.chinaunicom.permission.entity.RoleMenu;
import org.springframework.stereotype.Repository;

@Repository("roleMenuDao")
public class RoleMenuDaoImpl extends BaseDaoImpl<RoleMenu> implements RoleMenuDao{

	@Override
	public List<Map<String, Object>> queryRoleMenu(int rid) {
		String sql = "SELECT m.*, rm.rid FROM menu m LEFT JOIN role_menu rm ON rm.mid = m.mid AND rm.rid = ? ";
		List<Map<String, Object>> roleMenus =  jdbcTemplate.queryForList(sql, rid);
		return roleMenus;
	}

}
