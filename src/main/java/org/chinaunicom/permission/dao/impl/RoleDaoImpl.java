package org.chinaunicom.permission.dao.impl;

import org.chinaunicom.permission.dao.RoleDao;
import org.chinaunicom.permission.entity.Role;
import org.springframework.stereotype.Repository;

@Repository("roleDao")
public class RoleDaoImpl extends BaseDaoImpl<Role> implements RoleDao {

}
