package org.chinaunicom.permission.dao.impl;

import org.chinaunicom.permission.dao.MenuDao;
import org.chinaunicom.permission.entity.Menu;
import org.springframework.stereotype.Repository;

@Repository("menuDao")
public class MenuDaoImpl extends BaseDaoImpl<Menu> implements MenuDao {

}
