package org.chinaunicom.permission.dao;

import java.util.List;
import java.util.Map;

import org.chinaunicom.permission.entity.UserRole;

public interface UserRoleDao extends BaseDao<UserRole> {

	public List<Map<String, Object>> queryRoleUser(int rid);
}
