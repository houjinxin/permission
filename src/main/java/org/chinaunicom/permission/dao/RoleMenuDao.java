package org.chinaunicom.permission.dao;

import java.util.List;
import java.util.Map;

import org.chinaunicom.permission.entity.RoleMenu;

public interface RoleMenuDao extends BaseDao<RoleMenu> {

	public List<Map<String, Object>> queryRoleMenu(int rid);
}
