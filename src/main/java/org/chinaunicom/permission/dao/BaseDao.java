package org.chinaunicom.permission.dao;

import java.util.List;

public interface BaseDao <T>{
	
	public void createEntity(T t);
	
	public void updateEntity(T t);
	
	public void deleteEntity(T t);
	
	public T queryEntityById(int id);
	
	public List<T> queryEntityList();
	
}
