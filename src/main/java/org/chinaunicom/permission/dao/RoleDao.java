package org.chinaunicom.permission.dao;

import org.chinaunicom.permission.entity.Role;

public interface RoleDao extends BaseDao<Role> {

}
