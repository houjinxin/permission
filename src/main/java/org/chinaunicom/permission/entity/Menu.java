package org.chinaunicom.permission.entity;

public class Menu {

	private int mid;
	private int parent_menu_mid;
	private String menu_name;
	private String menu_url;

	public int getMid() {
		return mid;
	}

	public void setMid(int mid) {
		this.mid = mid;
	}

	public int getParent_menu_mid() {
		return parent_menu_mid;
	}

	public void setParent_menu_mid(int parent_menu_mid) {
		this.parent_menu_mid = parent_menu_mid;
	}

	public String getMenu_name() {
		return menu_name;
	}

	public void setMenu_name(String menu_name) {
		this.menu_name = menu_name;
	}

	public String getMenu_url() {
		return menu_url;
	}

	public void setMenu_url(String menu_url) {
		this.menu_url = menu_url;
	}

}
