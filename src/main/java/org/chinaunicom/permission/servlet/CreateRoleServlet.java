package org.chinaunicom.permission.servlet;

import java.io.IOException;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.chinaunicom.permission.dao.RoleDao;
import org.chinaunicom.permission.entity.Role;
import org.springframework.stereotype.Component;

@Component
public class CreateRoleServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Logger logger = Logger.getLogger(CreateRoleServlet.class);  
	
	@Resource
	RoleDao roleDao;
	
	public CreateRoleServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String role_name = request.getParameter("role_name");
		String role_desc = request.getParameter("role_desc");
		Role role = new Role();
		role.setRole_name(role_name);
		role.setRole_desc(role_desc);
		roleDao.createEntity(role);
	}

}
