package org.chinaunicom.permission.servlet;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.chinaunicom.permission.dao.UserRoleDao;
import org.chinaunicom.permission.utils.CommonUtil;
import org.springframework.stereotype.Component;

@Component
public class QueryRoleUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	@Resource
	UserRoleDao userRoleDao;
	
    public QueryRoleUserServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int rid = Integer.valueOf(request.getParameter("rid"));
		List<Map<String,Object>> list = userRoleDao.queryRoleUser(rid);
		String jsonStr = "[";
		for (Map map : list) {
			jsonStr +="{\"id\":\""+ map.get("uid") + "\",\"pId\":\"0" +"\",\"name\":\""+ map.get("username") + "\"},";
		}
		jsonStr = jsonStr.substring(0, jsonStr.length()-1)+"]";
		CommonUtil.returnJson(response, jsonStr);
	}

}
