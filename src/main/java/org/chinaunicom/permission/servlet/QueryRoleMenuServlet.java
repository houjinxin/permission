package org.chinaunicom.permission.servlet;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.chinaunicom.permission.dao.RoleMenuDao;
import org.chinaunicom.permission.utils.CommonUtil;
import org.springframework.stereotype.Component;

@Component
public class QueryRoleMenuServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	@Resource
	RoleMenuDao roleMenuDao;
	
    public QueryRoleMenuServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int rid = Integer.valueOf(request.getParameter("rid"));
		List<Map<String,Object>> maps = roleMenuDao.queryRoleMenu(rid);
		String jsonStr = "[";
		for (Map map : maps) {
			jsonStr +="{\"id\":\""+ map.get("mid") + "\",\"pId\":\"" + map.get("parent_menu_mid") +
							  "\",\"name\":\""+ map.get("menu_name") + "\",\"url\":\"" + map.get("menu_url") + 
							  "\",\"checked\":\"" + (map.get("rid")!= null && !map.get("rid").toString().equals("") ? "true" : "false" ) + "\"},";
 
		}
		jsonStr = jsonStr.substring(0, jsonStr.length()-1)+"]";
		CommonUtil.returnJson(response, jsonStr);
	}

}
