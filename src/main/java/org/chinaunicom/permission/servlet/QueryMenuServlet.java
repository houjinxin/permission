package org.chinaunicom.permission.servlet;

import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.chinaunicom.permission.dao.MenuDao;
import org.chinaunicom.permission.entity.Menu;
import org.chinaunicom.permission.utils.CommonUtil;
import org.springframework.stereotype.Component;

@Component
public class QueryMenuServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Logger logger = Logger.getLogger(QueryMenuServlet.class);         

	@Resource
	MenuDao menuDao;
	
    public QueryMenuServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		List<Menu> menus = menuDao.queryEntityList();
		String jsonStr = "[";
		for (Menu menu : menus) {
			jsonStr +="{\"id\":\""+ menu.getMid() +"\",\"pId\":\""+ menu.getParent_menu_mid() +"\",\"name\":\"" + menu.getMenu_name() +"\",\"url\":\"" + menu.getMenu_url() + "\"},";
		}
		jsonStr = jsonStr.substring(0, jsonStr.length()-1)+"]";
		CommonUtil.returnJson(response, jsonStr);
	}

}
