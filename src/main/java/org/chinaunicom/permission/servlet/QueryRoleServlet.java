package org.chinaunicom.permission.servlet;

import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.chinaunicom.permission.dao.RoleDao;
import org.chinaunicom.permission.entity.Menu;
import org.chinaunicom.permission.entity.Role;
import org.chinaunicom.permission.utils.CommonUtil;
import org.springframework.stereotype.Component;

@Component
public class QueryRoleServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	@Resource
	RoleDao roleDao;
	
    public QueryRoleServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Role> roles = roleDao.queryEntityList();
		String jsonStr = "[{\"id\":\"0\",\"name\":\"角色列表\",\"pId\":\"0\",\"open\":\"true\"},";
		for (Role role : roles) {
			jsonStr +="{\"id\":\""+ role.getRid() +"\",\"name\":\""+ role.getRole_name() +"\",\"pId\":\"0\"},";
		}
		jsonStr = jsonStr.substring(0, jsonStr.length()-1)+"]";
		CommonUtil.returnJson(response, jsonStr);
	}

}
