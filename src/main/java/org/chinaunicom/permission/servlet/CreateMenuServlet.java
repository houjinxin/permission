package org.chinaunicom.permission.servlet;

import java.io.IOException;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.chinaunicom.permission.dao.MenuDao;
import org.chinaunicom.permission.entity.Menu;
import org.chinaunicom.permission.utils.CommonUtil;
import org.springframework.stereotype.Component;

@Component
public class CreateMenuServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Logger logger = Logger.getLogger(CreateMenuServlet.class);  
	
	@Resource
	MenuDao menuDao;
	
    public CreateMenuServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String menu_name = request.getParameter("menu_name");
		int parent_menu_mid = Integer.valueOf(request.getParameter("parent_menu_mid"));
		String menu_url = request.getParameter("menu_url");
		Menu menu = new Menu();
		menu.setMenu_name(menu_name);
		menu.setParent_menu_mid(parent_menu_mid);
		menu.setMenu_url(menu_url);
		menuDao.createEntity(menu);
	}

}
