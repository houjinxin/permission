package org.chinaunicom.permission.servlet;

import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.chinaunicom.permission.dao.RoleDao;
import org.chinaunicom.permission.dao.UserDao;
import org.chinaunicom.permission.entity.Menu;
import org.chinaunicom.permission.entity.User;
import org.chinaunicom.permission.utils.CommonUtil;
import org.springframework.stereotype.Component;

@Component
public class QueryUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	@Resource
	UserDao userDao;
	
    public QueryUserServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<User> users = userDao.queryEntityList();
		String jsonStr = "[";
		for (User user : users) {
			jsonStr +="{\"id\":\""+ user.getUid() +"\",\"pId\":\"0\",\"name\":\"" + user.getUsername() + "\"},";
		}
		jsonStr = jsonStr.substring(0, jsonStr.length()-1)+"]";
		CommonUtil.returnJson(response, jsonStr);
	}

}
