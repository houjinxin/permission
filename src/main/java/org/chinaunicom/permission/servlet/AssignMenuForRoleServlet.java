package org.chinaunicom.permission.servlet;

import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.chinaunicom.permission.dao.RoleMenuDao;
import org.chinaunicom.permission.entity.RoleMenu;
import org.springframework.stereotype.Component;

@Component
public class AssignMenuForRoleServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	@Resource
	RoleMenuDao roleMenuDao;
	
    public AssignMenuForRoleServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String roles = request.getParameter("roles");
		String menus = request.getParameter("menus");
		String[] rids = roles.split(",");
		String[] mids = menus.split(",");
		for (String rid : rids) {
			List<RoleMenu> roleMenus = roleMenuDao.queryEntityList();
			for (String mid : mids) {
				RoleMenu roleMenu = new RoleMenu();
				roleMenu.setRid(Integer.valueOf(rid));
				roleMenu.setMid(Integer.valueOf(mid));
				roleMenuDao.createEntity(roleMenu);
			}
		}
	}

}
