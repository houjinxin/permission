package org.chinaunicom.permission.utils;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

public class CommonUtil {
	
	private static Logger logger = Logger.getLogger(CommonUtil.class);
	
	/**
	 * 用于将json串返回前台解析
	 * @param response
	 * @param jsonStr
	 */
	public static void returnJson(HttpServletResponse response,String jsonStr)
	{
		response.setCharacterEncoding("UTF-8");
		response.setContentType("appliction/json");
		PrintWriter out = null;
		try {
			out = response.getWriter();
			out.write(jsonStr);
			out.flush();
		} catch (IOException e) {
			logger.error(e);
		}
	}
}
