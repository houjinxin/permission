		function zTreeOnCheck(event, treeId, treeNode){
// 			 alert(treeNode.tId + ", " + treeNode.name + "," + treeNode.checked);
		}
		
		function zTreeBeforeCheck(treeId, treeNode) {
// 			alert(treeNode.tId + ", " + treeNode.name );
		};
		
		$(function(){
				var setting = {	
						check: {
							enable: true
						},
						data: {
							simpleData: {
								enable: true
							}
						},
						callback: {
							onCheck: zTreeOnCheck,
							beforeCheck: zTreeBeforeCheck
						}
				};
		
				$.ajax({
					url : 'queryRoleMenu',
					data: {"rid" : window.$('#rid').val() == undefined ? 1 : window.$('#rid').val() },
					dataType: 'json',
					success : function(response){
						console.log(response);
						$.fn.zTree.init($("#menuTree"), setting, response);
						$.fn.zTree.getZTreeObj("menuTree").expandAll(true);
					}
				});
		});