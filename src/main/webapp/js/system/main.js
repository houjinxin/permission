$(function() {
		var tabs = $("#tabs").tabs({
			active: 0,
			collapsible: false,
			event: "mouseover",
			heightStyle: "content"
		});
		
		var setting = {	
				check: {
					enable: false
				},
				data: {
					simpleData: {
						enable: true
					}
				},
				callback: {
					onCheck: zTreeOnCheck,
					beforeCheck: zTreeBeforeCheck,
					onClick: treeNodeOnClick
				}
		};
		$.ajax({
			url : 'queryRole',
				dataType : 'json',
				success : function(response) {
						$.each(response,function(i, v) {
							$.fn.zTree.init($("#roleList"), setting, response);
						});
				}
			});
		
		function zTreeOnCheck(event, treeId, treeNode){
//	 		alert(treeNode.tId + ", " + treeNode.name + "," + treeNode.level);
		}
		
		function zTreeBeforeCheck(treeId, treeNode) {
//			alert(treeNode.tId + ", " + treeNode.name );
		};
		
		function treeNodeOnClick(event, treeId, treeNode){
			$('#rid').val(treeNode.id);
			tabs.tabs('load', tabs.tabs( "option", "active"));
		}
	});