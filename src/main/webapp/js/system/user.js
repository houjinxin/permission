$(document).ready(function(){
			var setting = {	
					check: {
						enable: true
					},
					data: {
						simpleData: {
							enable: true
						}
					},
					callback: {
						onCheck: zTreeOnCheck,
						beforeCheck: zTreeBeforeCheck
					}
			};
			
			$.ajax({
					url : 'queryRoleUser',
					type : 'POST',
					data : {"rid": window.$('#rid').val() == undefined ? 1 : window.$('#rid').val() },
					dataType : 'json',
					success : function(response) {
						$.fn.zTree.init($("#userTree"), setting, response);
					}
			});
		});
		
		function zTreeOnCheck(event, treeId, treeNode){
//			 alert(treeNode.tId + ", " + treeNode.name + "," + treeNode.checked);
		}
		
		function zTreeBeforeCheck(treeId, treeNode) {
//			alert(treeNode.tId + ", " + treeNode.name );
		};