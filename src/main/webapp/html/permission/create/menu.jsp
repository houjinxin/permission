<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>创建菜单</title>
</head>
<body>
	<form id="createMenuForm" action="createMenu" method="post">
		<table>
			<tr>
				<td>菜单名称</td>
				<td>
					<input type="text" name="menu_name" id="menu_name">
				</td>
			</tr>
			<tr>
				<td>父级菜单</td>
				<td>
					<input type="text" name="parent_menu_mid" id="parent_menu_mid">
				</td>
			</tr>
			<tr>
				<td>菜单URL</td>
				<td>
					<input type="text" name="menu_url" id="menu_url">
				</td>
			</tr>
			<tr>
				<td>
					<input type="submit" value="创建菜单" id="createMenu">
				</td>
				<td></td>
			</tr>
		</table>
	</form>
</body>
</html>