<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>创建角色</title>
</head>
<body>
	<form id="createRoleForm" action="createRole" method="post">
		<table>
			<tr>
				<td>角色名称</td>
				<td>
					<input type="text" name="role_name" id="role_name">
				</td>
			</tr>
			<tr>
				<td>角色描述</td>
				<td>
					<textarea rows="3" cols="10" name="role_desc" id="role_desc"></textarea>
				</td>
			</tr>
			<tr>
				<td>
					<input type="submit" value="创建角色" id="createRole">
				</td>
				<td></td>
			</tr>
		</table>
	</form>
</body>
</html>