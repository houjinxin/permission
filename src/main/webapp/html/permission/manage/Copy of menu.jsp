<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>菜单管理</title>
<link rel="stylesheet" href="js/zTree_v3/css/zTreeStyle/zTreeStyle.css"
	type="text/css">
<script type="text/javascript" src="js/zTree_v3/js/jquery-1.4.4.min.js"></script>
<script type="text/javascript"
	src="js/zTree_v3/js/jquery.ztree.core-3.5.js"></script>
<script type="text/javascript"
	src="js/zTree_v3/js/jquery.ztree.excheck-3.5.js"></script>
<script type="text/javascript">
		function zTreeOnCheck(event, treeId, treeNode){
// 			 alert(treeNode.tId + ", " + treeNode.name + "," + treeNode.checked);
		}
		
		function zTreeBeforeCheck(treeId, treeNode) {
// 			alert(treeNode.tId + ", " + treeNode.name );
		};
		
		var setting = {	
				check: {
					enable: true
				},
				data: {
					simpleData: {
						enable: true
					}
				},
				callback: {
					onCheck: zTreeOnCheck,
					beforeCheck: zTreeBeforeCheck
				}
		};

		$.ajax({
			url : '<%=basePath%>'+'queryMenu',
			dataType: 'json',
			success : function(response){
				console.log(response);
				$.fn.zTree.init($("#menuTree"), setting, response);
			}
		});
		$.ajax({
			url : '<%=basePath%>' + 'queryRole',
				dataType : 'json',
				success : function(response) {
					var tr = '';
					$
							.each(
									response,
									function(i, v) {
										tr += '<tr>\
								<td>\
									<input id="rid" name="rid" class="checkBox" value="' + v.rid + '" type="checkbox" title="' + v.role_name + '"/>\
								</td>\
								<td>'
												+ v.role_name
												+ '</td>\
								<td>'
												+ v.role_desc
												+ '</td>\
							</tr>';
									});

					$('#roleTable').append(tr);
				}
			});

	$(function() {
		$('#checkAll').click(function() {
			if ($('#checkAll').attr('checked')) {
				$('.checkBox').attr('checked', true);
			} else {
				$('.checkBox').attr('checked', false);
			}
		});
		assignMenuForRole();
	});

	function assignMenuForRole() {
		$('#assignMenuForRole').click(function(){
			if(getAllRols() && getMenuNodes())
				$('#assignMenuForRoleForm').submit();
		});
	}

	function getAllRols(){
		var roleStr = "";
		if ( $('.checkBox:checked').length <= 0) {
			//没有选中
			alert("请选择角色");
			return false;
		} else {
			for(var i = 0 ; i < $('.checkBox:checked').length; i++)
			{
				if (i == 0) {
					roleStr = $('.checkBox:checked')[i].value;
				} else {
					roleStr += "," + $('.checkBox:checked')[i].value;
				}
			}
		}
		$('#roles').val(roleStr);
		return true;
	}
	
	function getMenuNodes() {
		var mNodes;
		var menuStr = "";
		//获取菜单数据
		var menuTreeObj = $.fn.zTree.getZTreeObj("menuTree");
		mNodes = menuTreeObj.getCheckedNodes(true);
		if (mNodes.length <= 0) {
			//没有选中
			alert("请选择菜单");
			return false;
		} else {
			for (var i = 0; i < mNodes.length; i++) {
				if (i == 0) {
					menuStr = mNodes[i].id;
				} else {
					menuStr += "," + mNodes[i].id;
				}
			}
		}
		$('#menus').val(menuStr);
		return true;
	}
</script>
</head>
<body>
	<div>
		<ul id="menuTree" class="ztree"></ul>
	</div>
	<div>
		<table id="roleTable">
			<tr>
				<td><input id="checkAll" class="chooseOrNot" type="checkbox"
					title="全选/反选" /></td>
				<td>角色名称</td>
				<td>角色描述</td>
			</tr>
		</table>
		<form id="assignMenuForRoleForm" action="assignMenuForRole" method="post">
			<input id="menus" type="hidden" name="menus" value=""> 
			<input id="roles" type="hidden" name="roles" value=""> 
			<input  id="assignMenuForRole" type="button" title="分配角色菜单" value="分配角色菜单" />
		</form>
	</div>
</body>
</html>