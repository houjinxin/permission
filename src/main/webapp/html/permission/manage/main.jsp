<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>菜单管理</title>
<link rel="stylesheet" href="js/zTree_v3/css/zTreeStyle/zTreeStyle.css" type="text/css">
<link rel="stylesheet" href="js/jquery-ui-1.10.4.custom/development-bundle/themes/base/jquery.ui.all.css">
<link rel="stylesheet" href="css/demos.css">
<script src="js/jquery-ui-1.10.4.custom/js/jquery-1.10.2.js"></script>
<script src="js/jquery-ui-1.10.4.custom/development-bundle/ui/jquery.ui.core.js"></script>
<script src="js/jquery-ui-1.10.4.custom/development-bundle/ui/jquery.ui.widget.js"></script>
<script src="js/jquery-ui-1.10.4.custom/development-bundle/ui/jquery.ui.mouse.js"></script>
<script src="js/jquery-ui-1.10.4.custom/development-bundle/ui/jquery.ui.selectable.js"></script>
<script src="js/jquery-ui-1.10.4.custom/development-bundle/ui/jquery.ui.tabs.js"></script>
<script src="js/jquery-ui-1.10.4.custom/development-bundle/ui/jquery.ui.button.js"></script>

<script type="text/javascript" src="js/zTree_v3/js/jquery.ztree.core-3.5.js"></script>
<script type="text/javascript" src="js/zTree_v3/js/jquery.ztree.excheck-3.5.js"></script>

<script type="text/javascript" src="js/system/main.js"></script>
</head>
<body>
	<div id="top">
	
	</div>
	<div id="left" class="demo-left ui-widget-content ui-widget-content ui-corner-top">
		<div class="demo-header ui-widget-header  ui-widget-content ui-corner-top">
			角色列表
		</div>
		<div class="ui-widget-content">
			<div>
				<ul id="roleList" class="ztree"></ul>
			</div>
		</div>
	</div>
	<div id="right" class="demo-right">
		<div id="tabs">
			<ul>
				<li><a href="html/permission/manage/user.jsp">角色用户</a></li>
				<li><a href="html/permission/manage/menu.jsp">角色菜单</a></li>
				<li><a href="html/permission/manage/role.jsp">角色权限</a></li>
			</ul>
		</div>
	</div>
    <div id="hiddenarea">
    	<input type="hidden" name="uid" id="uid" value="">
        <input type="hidden" name="activeTab" id="activeTab" value="">
        <input type="hidden" name="rid" id="rid" value="1">
    </div>
</body>
</html>