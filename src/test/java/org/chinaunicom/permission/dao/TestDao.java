package org.chinaunicom.permission.dao;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class TestDao {
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	public void initDb()
	{
		String sql = "drop table if exists test_table;";
		jdbcTemplate.execute(sql);
		sql = "create table test_table(id int primary key,name varchar(60));";
		jdbcTemplate.execute(sql);
	}
	
}
