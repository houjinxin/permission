package org.chinaunicom.permission.test;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.chinaunicom.permission.dao.MenuDao;
import org.chinaunicom.permission.dao.RoleDao;
import org.chinaunicom.permission.dao.RoleMenuDao;
import org.chinaunicom.permission.dao.TestDao;
import org.chinaunicom.permission.dao.UserDao;
import org.chinaunicom.permission.dao.UserRoleDao;
import org.chinaunicom.permission.entity.Menu;
import org.chinaunicom.permission.entity.Role;
import org.chinaunicom.permission.entity.RoleMenu;
import org.chinaunicom.permission.entity.User;
import org.chinaunicom.permission.entity.UserRole;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Client {

	private static ApplicationContext applicationContext;
	private static final String CONFIGPATH = "applicationContext.xml";

	@Test
	public void testInitDb() {
		  applicationContext = new ClassPathXmlApplicationContext(CONFIGPATH);
		  TestDao testDao = (TestDao) applicationContext.getBean("testDao");
		  testDao.initDb();
	}
	
	@Test
	public void testCreateEntity() {
		  applicationContext = new ClassPathXmlApplicationContext(CONFIGPATH);
		  UserDao userDao = (UserDao) applicationContext.getBean("userDao");
		  User user = new User();
		  user.setUsername("houjinxin");
		  user.setPassword("houjinxin");
		  userDao.createEntity(user);
		  
		  MenuDao menuDao = (MenuDao) applicationContext.getBean("menuDao");
		  Menu menu = new Menu();
		  menu.setParent_menu_mid(0);
		  menu.setMenu_name("百度一下");
		  menu.setMenu_url("http://www.baidu.com");
		  menuDao.createEntity(menu);
		  
		  RoleDao roleDao = (RoleDao) applicationContext.getBean("roleDao");
		  Role role = new Role();
		  role.setRole_name("角色1");
		  role.setRole_desc("角色菜单");
		  roleDao.createEntity(role);
		  
		  UserRoleDao userRoleDao = (UserRoleDao) applicationContext.getBean("userRoleDao");
		  UserRole userRole = new UserRole();
		  userRole.setRid(1);
		  userRole.setUid(1);
		  userRoleDao.createEntity(userRole);
		  
		  RoleMenuDao roleMenuDao = (RoleMenuDao) applicationContext.getBean("roleMenuDao");
		  RoleMenu roleMenu = new RoleMenu();
		  roleMenu.setMid(1);
		  roleMenu.setRid(1);
		  roleMenuDao.createEntity(roleMenu);
	}
	
	@Test
	public void testQueryEntityById(){
		applicationContext = new ClassPathXmlApplicationContext(CONFIGPATH);
		UserDao userDao = (UserDao) applicationContext.getBean("userDao");
		MenuDao menuDao = (MenuDao) applicationContext.getBean("menuDao");
		RoleDao roleDao = (RoleDao) applicationContext.getBean("roleDao");
		  UserRoleDao userRoleDao = (UserRoleDao) applicationContext.getBean("userRoleDao");
		  RoleMenuDao roleMenuDao = (RoleMenuDao) applicationContext.getBean("roleMenuDao");
		
		User user = userDao.queryEntityById(1);
		Menu menu = menuDao.queryEntityById(1);
		Role role = roleDao.queryEntityById(1);
		UserRole userRole = userRoleDao.queryEntityById(1);
		RoleMenu roleMenu = roleMenuDao.queryEntityById(1);
	}

	@Test
	public void testUpdateEntity(){
		applicationContext = new ClassPathXmlApplicationContext(CONFIGPATH);
		UserDao userDao = (UserDao) applicationContext.getBean("userDao");
		MenuDao menuDao = (MenuDao) applicationContext.getBean("menuDao");
		RoleDao roleDao = (RoleDao) applicationContext.getBean("roleDao");
		UserRoleDao userRoleDao = (UserRoleDao) applicationContext.getBean("userRoleDao");
		RoleMenuDao roleMenuDao = (RoleMenuDao) applicationContext.getBean("roleMenuDao");
		
		
		User user = userDao.queryEntityById(1);
		user.setUsername("houjinxin");
		userDao.updateEntity(user);
		
		Role role = roleDao.queryEntityById(1);
		role.setRole_name("角色5");
		roleDao.updateEntity(role);
		
		Menu menu = menuDao.queryEntityById(1);
		menu.setMenu_name("菜单一");
		menu.setMenu_url("http://taobao2.com");
		menuDao.updateEntity(menu);
		
		UserRole userRole = userRoleDao.queryEntityById(1);
		userRole.setUid(2);
		userRole.setRid(2);
		userRoleDao.updateEntity(userRole);
		
		RoleMenu roleMenu = roleMenuDao.queryEntityById(1);
		roleMenu.setRid(2);
		roleMenu.setMid(2);
		roleMenuDao.updateEntity(roleMenu);
	}
	
	@Test
	public void testQueryEntityList(){
		applicationContext = new ClassPathXmlApplicationContext(CONFIGPATH);
		UserDao userDao = (UserDao) applicationContext.getBean("userDao");
		MenuDao menuDao = (MenuDao) applicationContext.getBean("menuDao");
		RoleDao roleDao = (RoleDao) applicationContext.getBean("roleDao");
		
		List<Menu> menuList = menuDao.queryEntityList();
		List<User> userList = userDao.queryEntityList();
		List<Role> roleList = roleDao.queryEntityList();
	}
	
	@Test
	public void testQueryRoleMenu(){
		applicationContext = new ClassPathXmlApplicationContext(CONFIGPATH);
		RoleMenuDao roleMenuDao = (RoleMenuDao) applicationContext.getBean("roleMenuDao");
		List<Map<String,Object>> lists = roleMenuDao.queryRoleMenu(1);
		for (Map<String, Object> map : lists) {
			for (String key : map.keySet()) {
				System.out.println(key + ":" + map.get(key));
			}
		}
	}
}
